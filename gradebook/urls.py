from django.urls import path
from . import views
urlpatterns = [
    path('student/<int:rollno>', views.student_detail, name='student_detail'),
    path('student/add', views.add_student, name='add_student'),
    path('student/<int:rollno>/addmark', views.add_student_mark, name='add_student_mark'),
    path('student/bulk-upload-form', views.bulk_upload_form, name='bulk_upload_form'),
    path('student/bulk-upload', views.student_bulk_upload, name='student_bulk_upload'),
    path('student/all', views.student_list, name='student_list'),
    path('login', views.login_view, name='login'),
    path('logout', views.logout_view, name='logout')
]
