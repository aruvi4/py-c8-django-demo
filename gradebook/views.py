from django.shortcuts import render, redirect
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponse
from .models import Student, Marks
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required, permission_required
from .forms import StudentForm
# Create your views here.

@login_required(login_url='login')
def student_detail(request, rollno):
    s = Student.objects.get(rollno=rollno)
    context = {
        'student': s
    }
    return render(request, 'gradebook/student_detail.html', context)

@login_required(login_url='login')
def add_student(request):
    if request.method == 'POST':
        sf = StudentForm(request.POST)
        if sf.is_valid():
            student = Student.objects.create(
                name=sf.cleaned_data['name'],
                rollno=sf.cleaned_data['rollno'],
            )
            return redirect('student_detail', student.rollno)    
    else:
        sf = StudentForm()
    return render(request, 'gradebook/student_form.html', {'form': sf})   
            
    

@permission_required('gradebook.add_marks', login_url='login')
def add_student_mark(request, rollno):
    if request.method == 'GET':
        return HttpResponse("please use POST only on this endpoint")
    s = Student.objects.get(rollno=rollno)
    subject = request.POST['subject']
    score = request.POST['score']
    Marks.objects.create(
        student=s,
        subject=subject,
        score=score
    )
    return redirect('student_detail', s.rollno)

@login_required(login_url='login')
def student_bulk_upload(request):
    uploaded_file = request.FILES['bulkmarks']
    contents = str(uploaded_file.read())
    lines = contents.split('\\n')
    lines[0] = lines[0][2:]
    del lines[-1]
    for line in lines:
        contents = line.split(';')
        rollno = contents[0]
        name = contents[1]
        s, created = Student.objects.get_or_create(name=name, rollno=rollno)
        for item in contents[2:]:
            item = item.replace(' ', '')
            item = item.replace('(', '')
            item = item.replace(')', '')
            subject, score = item.split(',')
            Marks.objects.get_or_create(student=s, subject=subject, score=score)
    return HttpResponse(lines)

@login_required(login_url='login')
def bulk_upload_form(request):
    return render(request, 'gradebook/bulk_student_upload.html')

@login_required(login_url='login')
def student_list(request):
    s = Student.objects.all()
    return render(request, 'gradebook/allstudents.html', {'students': s})

def login_view(request):
    if request.method == 'GET':
        return render(request, 'gradebook/login.html')
    if request.method == 'POST':
        username = request.POST.get('username', "")
        password = request.POST.get('password', "")
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            redirect_url = request.POST.get('next')
            print(redirect_url)
            if redirect_url not in ['', None]:
                return redirect(redirect_url)
            return redirect('student_list')
        else:
            return render(request, 'gradebook/login.html', {'message': 'Invalid credentials, try again'})
def logout_view(request):
    logout(request)
    return redirect('login')