from django.db import models

# Create your models here.

class Student(models.Model):
    name = models.CharField(max_length=256)
    rollno = models.IntegerField()

    def total_marks(self):
        marks = Marks.objects.filter(student=self)
        return sum([mark.score for mark in marks])
    
    def grade(self):
        grade = 'UNDEFINED'
        marks = Marks.objects.filter(student=self)
        if len(marks) != 0:
            avg = self.total_marks() / len(marks)
            grades = {(90, 100): 'A', (80, 90): 'A-', (70, 80): 'B', (60, 70): 'B-', (40, 60): 'C', (0, 40): 'F'}
            for lower, upper in grades.keys():
                if lower < avg <= upper:
                    grade = grades[(lower, upper)]
        return grade
        

class Marks(models.Model):
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    score = models.IntegerField()
    subject = models.CharField(max_length=256)