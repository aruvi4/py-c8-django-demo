from django.contrib import admin
from gradebook.models import Student, Marks
from django.contrib.auth.models import Permission

# Register your models here.
admin.site.register(Student)
admin.site.register(Marks)
admin.site.register(Permission)