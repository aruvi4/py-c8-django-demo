from django import forms
from django.core.exceptions import ValidationError

class StudentForm (forms.Form):
    name = forms.CharField(label='Student name', max_length=256)
    rollno = forms.IntegerField(label='Roll number', max_value=1000000)

    def clean(self):
        roll_data = self.cleaned_data['rollno']
        name_data = self.cleaned_data['name']
        if name_data == 'dinesh' and roll_data != 7:
            raise ValidationError('Dinesh is only 007')