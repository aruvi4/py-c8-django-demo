from django.urls import path
from . import views

app_name='blog'

urlpatterns = [
    path('myfirstpage', views.first_page, name='myfirstpage'),
    path('genericpage', views.generic_page, name='genericpage'),
    path('fibonacci/<int:num>', views.fibonacci_finder, name='fibonacci'),
    path('blogform', views.show_blog_form, name='show_blog_form'),
    path('blogform/addnew', views.add_blog_entry, name='add_blog_entry'),
    path('posts/<int:pk>', views.get_post, name='get_post'),
    path('allposts', views.get_all_posts, name='get_all_posts')
]