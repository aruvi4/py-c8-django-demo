from django.db import models

# Create your models here.

class Post(models.Model):
    title = models.CharField(max_length=256)
    heading = models.CharField(max_length=1000)
    text = models.TextField(max_length=100000)

    def __repr__(self):
        return f'Title: {self.title}, Heading:{self.heading}'