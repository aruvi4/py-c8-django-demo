from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Post

# Create your views here.
def nth_fibo(n):
    if n == 1 or n == 2:
        return 1
    return nth_fibo(n - 1) + nth_fibo(n - 2)

def fibonacci_finder(request, num):
    x = [nth_fibo(n) for n in range(1, num + 1)]
    context = {'fibonaccis': x}
    return render(request, 'blog/fibonaccis.html', context)

def first_page(request):
    x = 2 ** 0.5
    context = {'topsecret': x}
    return render(request, 'blog/firstpage.html', context)

def generic_page(request):
    title = "Your title here"
    heading = "Your heading here"
    text = "Your text here"
    context = {'title': title, 'heading': heading, 'text': text}
    return render(request, 'blog/genericpage.html', context)

def show_blog_form(request):
    return render(request, 'blog/blogform.html')

def add_blog_entry(request):
    title = request.POST['title']
    heading = request.POST['heading']
    text = request.POST['text']
    p = Post(title=title, heading=heading, text=text)
    p.save()
    return redirect('blog:get_post', p.pk)

def get_post(request, pk):
    p = Post.objects.get(pk=pk)
    context = {'title': p.title, 'heading': p.heading, 'text': p.text}
    return render(request, 'blog/genericpage.html', context)

def get_all_posts(request):
    posts = Post.objects.all()
    return render(request, 'blog/allposts.html', {'posts': posts})